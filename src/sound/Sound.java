package sound;

import javax.swing.*;

import org.json.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.sound.sampled.*;

public class Sound extends JFrame {

	// sound storage
	private byte[] combined = new byte[8000 * 4 * 7];;
	private Recorder guitarRecorder;
	private Recorder drumRecorder;
	private Recorder bassRecorder;


	public Sound() {
		super("Capture Sound Demo");

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		Container content = getContentPane();

		// enable buttons
		Buttons.setButtons(true);

		// capture guitar button
		guitarRecorder = new Recorder();
		ActionListener guitarListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Buttons.setButtons(false);
				guitarRecorder.capture();
			}
		};
		Buttons.guitarBtn.addActionListener(guitarListener);
		content.add(Buttons.guitarBtn, BorderLayout.NORTH);

		// capture drum button
		drumRecorder = new Recorder();
		ActionListener drumListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Buttons.setButtons(false);
				drumRecorder.capture();
			}
		};
		Buttons.drumBtn.addActionListener(drumListener);
		content.add(Buttons.drumBtn, BorderLayout.EAST);

		// capture bass button
		bassRecorder = new Recorder();
		ActionListener bassListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Buttons.setButtons(false);
				bassRecorder.capture();
			}
		};
		Buttons.bassBtn.addActionListener(bassListener);
		content.add(Buttons.bassBtn, BorderLayout.WEST);

		// mixer
		ActionListener combineListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Buttons.setButtons(false);
				InputStream in;
				try {
					in = new FileInputStream("sample.json");
					JSONTokener tokener = new JSONTokener(in);
					JSONObject sequence = new JSONObject(tokener);

					for (String track : JSONObject.getNames(sequence)) {
						byte[] bytes = getSoundTrack(track);
						System.out.println("track is=" + track);
						JSONArray positions = sequence.getJSONArray(track);

						for (int p = 0; p < positions.length(); ++p) {
							System.out.println("position is=" + positions.getInt(p));
							int position = positions.getInt(p) * 32000 - 1;
							if (position < 0) {
								position = 0;
							}
							Mixer.mix(combined, bytes, position);
						}
					}
					Buttons.setButtons(true);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
			}
		};
		Buttons.combineBtn.addActionListener(combineListener);
		content.add(Buttons.combineBtn, BorderLayout.CENTER);

		ActionListener playListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Buttons.setButtons(false);
				playAudio();
			}
		};
		Buttons.playBtn.addActionListener(playListener);
		content.add(Buttons.playBtn, BorderLayout.SOUTH);
	}

	protected byte[] getSoundTrack(String track) {
		switch (track) {
		case "guitar":
			return guitarRecorder.getRecord().toByteArray();
		case "bass":
			return bassRecorder.getRecord().toByteArray();
		case "drum":
			return drumRecorder.getRecord().toByteArray();
		default:
			System.out.println("no sound track found");
			return null;
		}

	}

	private void playAudio() {
		try {
			byte audio[] = combined;
			InputStream input = new ByteArrayInputStream(audio);
			final AudioFormat format = getFormat();
			final AudioInputStream ais = new AudioInputStream(input, format, audio.length / format.getFrameSize());
			DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
			final SourceDataLine line = (SourceDataLine) AudioSystem.getLine(info);
			line.open(format);
			line.start();

			Runnable runner = new Runnable() {
				int bufferSize = (int) format.getSampleRate() * format.getFrameSize();
				byte buffer[] = new byte[bufferSize];

				public void run() {
					try {
						int count;
						while ((count = ais.read(buffer, 0, buffer.length)) != -1) {
							if (count > 0) {
								line.write(buffer, 0, count);
							}
						}
						line.drain();
						line.close();
						Buttons.setButtons(true);
					} catch (IOException e) {
						System.err.println("I/O problems: " + e);
						System.exit(-3);
					}
				}
			};
			Thread playThread = new Thread(runner);
			playThread.start();
		} catch (LineUnavailableException e) {
			System.err.println("Line unavailable: " + e);
			System.exit(-4);
		}
	}

	private AudioFormat getFormat() {
		float sampleRate = 8000;
		int sampleSizeInBits = 8;
		int channels = 1;
		boolean signed = true;
		boolean bigEndian = true;
		return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
	}

	public static void main(String args[]) {
		JFrame frame = new Sound();
		frame.pack();
		frame.show();
	}
}