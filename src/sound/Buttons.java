package sound;

import javax.swing.JButton;

public class Buttons {
	public static JButton guitarBtn = new JButton("Guitar");
	public static JButton drumBtn = new JButton("Drum");
	public static JButton bassBtn = new JButton("Bass");
	public static JButton combineBtn = new JButton("Combine");
	public static JButton playBtn = new JButton("Play");
	
	// button status setter
	public static void setButtons(boolean bool) {
		guitarBtn.setEnabled(bool);
		bassBtn.setEnabled(bool);
		drumBtn.setEnabled(bool);
		combineBtn.setEnabled(bool);
		playBtn.setEnabled(bool);
	}

}
