package sound;

public class Mixer {

	public static void mix(byte[] destination, byte[] add, int position) {
		for (int i = 0; i < add.length; i++) {
			int byteInt = 2 * (destination[position + i] + add[i]) 
					- (destination[position + i] * add[i] / 128) - 256;
			// simple noise gate 
			if (byteInt > -4 && byteInt < 4) {
				byteInt = 0;
			}
			destination[position + i] = (byte) byteInt;
		}
		
		//normalize(destination, position, position + add.length);
	}
	
	private static void normalize(byte[] bytes, int from, int to) {
		int max = 0;
		for (int i = 0; i < to; ++i) {
			int magnitude = Math.abs(bytes[i] - 128);
			if (magnitude > max) {
				max = magnitude;
			}
		}
		
		for (int i = 0; i < to; ++i) {
			bytes[i] = (byte) (((bytes[i] - 128) / max * 128) + 128);
		}
	}
}

