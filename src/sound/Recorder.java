package sound;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.sound.sampled.*;

public class Recorder {

	// global that stores the record as byte array
	private ByteArrayOutputStream record = new ByteArrayOutputStream();

	// constructor
	public Recorder() {

	}

	public ByteArrayOutputStream getRecord() {
		return record;
	}

	public void capture() {
		try {
			final AudioFormat format = getFormat();
			DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
			final TargetDataLine line = (TargetDataLine) AudioSystem.getLine(info);
			line.open(format);
			line.start();
			Runnable runner = new Runnable() {
				int bufferSize = (int) format.getSampleRate() * format.getFrameSize();
				byte buffer[] = new byte[bufferSize];

				public void run() {

					record = new ByteArrayOutputStream();

					long start = System.currentTimeMillis();
					long end = start + 4000;

					try {
						while (System.currentTimeMillis() < end) {
							int count = line.read(buffer, 0, buffer.length);
							if (count > 0) {
								record.write(buffer, 0, count);
							}
						}
						record.close(); // close output stream
						Buttons.setButtons(true);
					} catch (IOException e) {
						System.err.println("I/O problems: " + e);
						System.exit(-1);
					}
				}
			};
			Thread thread = new Thread(runner);
			thread.start();
		} catch (LineUnavailableException e) {
			System.err.println("Line unavailable: " + e);
			System.exit(-2);
		}
	}

	private AudioFormat getFormat() {
		float sampleRate = 8000;
		int sampleSizeInBits = 8;
		int channels = 1;
		boolean signed = true;
		boolean bigEndian = true;
		return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
	}
}
